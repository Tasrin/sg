<?php
	include('header.php');
?>
	<div class="bcumb" style="background-image: url('../resource/img/banner.jpg')">
		<div class="overlay">
			<div class="container text-center">
				<div class="bcumbarea">
					<h4>About Our Alumni</h4>
					<p>Alumni needs enable you to harness the power of your alumni network. Whatever may be the need.</p>
					<a href="#" class="btn btn-default abttop">Let's See</a>
				</div>
			</div>
		</div>
	</div>
	<div class="abtcontent">
		<div class="container">
			<div class="area">
				<h4 class="text-right">1966</h4>
				<div class="col-md-5"><img src="../resource/img/cu.jpg" alt="Image" class="img img-responsive"/></div>
				<strong>Chittagong University</strong>
				<p>
					The University of Chittagong is situated on 1,754 acres of beautiful hilly land in mauja Fatehpur under hathazari upazila, 22 km north of chittagong city. It was opened on 18 November 1966. Back in 1962, Mr. Mohammad Ferdaus Khan, Deputy Director of Public Instruction of East Pakistan, prepared a preliminary draft plan of Chittagong University. Prior to that the Chittagong University Movement Council was formed in 1961 with Badshah Mia Chowdhury, a distinguished social welfare worker of Chittagong, as president and professor Ahmad Hossain of Chittagong City College as convenor. Dr. muhammad shahidullah (1885-1969), Dr. muhammad enamul haq (1906-82) and some other intellectuals, educationists, political personalities, and social workers played leading roles in the site selection movement of 1962. On 3 December 1965, Professor a r mallick (1918-97), as project Director, inaugurated the work of founding the Chittagong University with only three employees, one typewriter, and a photocopier in a house on Road 3 of Chittagong City's Nasirabad Housing Society. The Chittagong University Ordinance was promulgated on 25 September 1966 and Professor Mallick was given the charge of Vice Chancellor on that very day. He resumed charge as the first Vice Chancellor of the university on 9 October 1966.
					<a target="_blank" style="text-decoration:none;" href="http://cu.ac.bd/ctguni/index.php?option=com_content&view=article&id=85&Itemid=205">Read More...</a>
				</p>
			</div>
			
			<!--<div class="area">
				<h4 class="text-right">2018</h4>
				<div style="float:right;" class="col-md-5"><img style="width:70%; height:auto;" src="resource/img/principle.png" alt="Image" class="img img-responsive"/></div>
				<strong>"Hello"</strong>
				<p>
					We welcome you all to the new website of The Chittagong University English Alumni Association. From now on, you will be regularly updated about the activities of the association and the English Department here. If you have received any degree, such as BA (Honours), MA, MPhil, or PhD from this department, you are one of our valued and proud alumni. We want this site to become a vibrant platform for the alumni across the globe to share their success stories which would inspire our existing students, and guide them to choose their professional pathways. Let’s grow together to serve the nation better.
				</p>
			</div>
			
			<div class="area">
				<h4 class="text-right">2006</h4>
				<div class="col-md-5"><img src="img/b1.png" alt="Image" class="resource/img img-responsive"/></div>
				<strong>There are many variations of passages</strong>
				<p>
					There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
					There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
				</p>
			</div>-->
			
		</div>
	</div>
	
<?php
	include('footer.php');
?>