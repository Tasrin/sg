<?php
use App\User\User;
use App\User\Auth;
use App\Rbac\Role;
use App\Rbac\PrivilegedUser;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$User = $obj->view();
$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

if(!$status) {
    Utility::redirect('User/Profile/signup.php');
    return;
}

$objController = new \App\Controller\Controller();
$singleUser=$objController->objectToArray($User);

############################### Session time calculation #####################################
$sessionMinute=$auth->sessionPeriod;
$sessionMinuteMultiply=$auth->sessionPeriodMultiply;
if(isset($_SESSION['expire'])) {
    $exp = $_SESSION['expire'];
    $now = time(); // Checking the time now when home page starts.
    $sub_exp = $now - $exp;
    if ($sub_exp > ($sessionMinute * $sessionMinuteMultiply)) {
        session_destroy();
        Utility::redirect('User/Profile/signup.php');
    }
    $_SESSION['expire'] = time();
    /* session timeout code end  */
}
################################ End of Session time calculation ##############################


$objBookTitle = new \App\ExpenseIncome\ExpenseIncome();
$objTransaction= new \App\ExpenseIncome\Transaction();
$objSearch= new \App\ExpenseIncome\Search();
$objBranch=new \App\ExpenseIncome\Branch();
$objBook=new \App\ExpenseIncome\Head();
$allData = $objTransaction->statement();
$allClients=$objBookTitle->allClients();
$accountHead=$objTransaction->accounthead();
$allparticulars=$objBookTitle->allparticulars();
$branches=$objBranch->branch();
$book=$objBook->book();

############################### Message Handle ################
$msg = Message::getMessage();
if(isset($_SESSION['mark']))  unset($_SESSION['mark']);
############################### Message Handle ################

################## search  block 1 of 5 start ##################
if(isset($_REQUEST['search']) )$someData =  $objSearch->search($_REQUEST);
$availableKeywords=$objSearch->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block 1 of 5 end ##################

//var_dump($availableKeywords);
######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);

if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;


$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objSearch->indexPaginator($page,$itemsPerPage);
$serial = (  ($page-1) * $itemsPerPage ) +1;
if($serial<1) $serial=1;
####################### pagination code block#1 of 2 end #########################################

################## search  block 2 of 5 start ##################
//$_REQUEST['search']="Photo";
if(isset($_REQUEST['search']) ) {
    $someData = $objSearch->search($_REQUEST);
    $serial = 1;
}
//var_dump($someData);
################## search  block 2 of 5 end ##################